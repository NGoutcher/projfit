package com.example.nathan.easyfit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class ProfileActivity extends AppCompatActivity {

    ImageButton HomeButton;
    Button ApplyButton;
    EditText NameText;
    EditText EmailText;
    EditText PhoneText;
    SharedPreferences savedNameText;
    SharedPreferences savedEmailText;
    SharedPreferences savedPhoneText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        HomeButton = findViewById(R.id.HomeButton);

        HomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        ApplyButton = (Button) findViewById(R.id.ApplyButton);
        NameText = (EditText) findViewById(R.id.editTextName);
        EmailText = (EditText) findViewById(R.id.editTextEmail);
        PhoneText = (EditText) findViewById(R.id.editTextPhone);

        savedNameText = getSharedPreferences("name", MODE_PRIVATE);
        savedEmailText = getSharedPreferences("email", MODE_PRIVATE);
        savedPhoneText = getSharedPreferences("phone", MODE_PRIVATE);

        NameText.setText(savedNameText.getString("tag", "Name"));
        EmailText.setText(savedEmailText.getString("tag", "Email"));
        PhoneText.setText(savedPhoneText.getString("tag", "Phone Number"));

        ApplyButton.setOnClickListener(saveButtonListener);
    }

    private void makeTag1(String tag) {
        String or = savedNameText.getString(tag, null);
        SharedPreferences.Editor preferencesEditor = savedNameText.edit();
        preferencesEditor.putString("tag",tag);
        preferencesEditor.commit();

    }

    private void makeTag2(String tag2) {
        String or2 = savedEmailText.getString(tag2, null);
        SharedPreferences.Editor preferencesEditor2 = savedEmailText.edit();
        preferencesEditor2.putString("tag",tag2);
        preferencesEditor2.commit();
    }

    private void makeTag3(String tag3) {
        String or3 = savedPhoneText.getString(tag3, null);
        SharedPreferences.Editor preferencesEditor3 = savedPhoneText.edit();
        preferencesEditor3.putString("tag",tag3);
        preferencesEditor3.commit();
    }

    public View.OnClickListener saveButtonListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            if(NameText.getText().length()>0){
                makeTag1(NameText.getText().toString());

                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(NameText.getWindowToken(),0);
            }

            if(EmailText.getText().length()>0){
                makeTag2(EmailText.getText().toString());

                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(EmailText.getWindowToken(),0);
            }

            if(PhoneText.getText().length()>0){
                makeTag3(PhoneText.getText().toString());

                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(PhoneText.getWindowToken(),0);
            }
        }
    };
}