package com.example.nathan.easyfit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class JournalActivity extends AppCompatActivity {

    ArrayList<String> JournalEntry = new ArrayList<>();
    TextView JournalEntryTextEmpty;
    ImageButton HomeButton;
    Button AddEntry;
    Button Apply;
    EditText JournalEntryText;
    ListView JournalList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal);

        HomeButton = findViewById(R.id.HomeButton);

        HomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(JournalActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        JournalEntryTextEmpty = findViewById(R.id.EmptyJournal);

        Apply = findViewById(R.id.applybutton);
        Apply.setOnClickListener(ApplyMethod);
        Apply.setVisibility(View.GONE);

        AddEntry = findViewById(R.id.addbutton);
        AddEntry.setOnClickListener(AddEntryMethod);

        JournalList = findViewById(R.id.journallist);

        JournalEntryText = findViewById(R.id.JournalTextEntry);


        DisplayEmptyText();
    }

    public void DisplayEmptyText() {
        if(JournalEntry.isEmpty()) {
            JournalEntryTextEmpty.setVisibility(View.VISIBLE);
            JournalList.setVisibility(View.INVISIBLE);
        }
        else {
            JournalList.setVisibility(View.VISIBLE);
        }
    }

    public View.OnClickListener AddEntryMethod = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            JournalEntryTextEmpty.setVisibility(View.INVISIBLE);
            JournalEntryText.setVisibility(View.VISIBLE);
            JournalList.setVisibility(View.INVISIBLE);

            AddEntry.setVisibility(View.GONE);
            Apply.setVisibility(View.VISIBLE);

            JournalEntryText.getText().clear();
        }
    };

    public View.OnClickListener ApplyMethod = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            JournalEntry.add(JournalEntryText.getText().toString());
            Apply.setVisibility(View.GONE);
            AddEntry.setVisibility(View.VISIBLE);
            JournalEntryText.setVisibility(View.INVISIBLE);

            DisplayEmptyText();

            ArrayAdapter<String> Adapter = new ArrayAdapter<String>(JournalActivity.this, android.R.layout.simple_list_item_1, JournalEntry);
            JournalList.setAdapter(Adapter);
        }
    };
}
