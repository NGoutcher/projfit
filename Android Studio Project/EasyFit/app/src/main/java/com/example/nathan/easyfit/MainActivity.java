package com.example.nathan.easyfit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    ImageButton ProfileIcon;
    ImageButton WorkoutIcon;
    ImageButton DietIcon;
    ImageButton JournalIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ProfileIcon = findViewById(R.id.ProfileIcon);

        ProfileIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
                finish();
            }
        });

        WorkoutIcon = findViewById(R.id.WorkoutIcon);

        WorkoutIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(MainActivity.this, WorkoutActivity.class);
                startActivity(intent);
                finish();
            }
        });

        DietIcon = findViewById(R.id.DietIcon);

        DietIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(MainActivity.this, DietActivity.class);
                startActivity(intent);
                finish();
            }
        });

        JournalIcon = findViewById(R.id.JournalIcon);

        JournalIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(MainActivity.this, JournalActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
