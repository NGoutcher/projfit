package com.example.nathan.easyfit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

public class WorkoutActivity extends AppCompatActivity {

    ImageButton HomeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        String[] workouts_category = {"Workout 1", "Workout 2", "Workout 3", "Workout 4", "Workout 5", "Workout 6", "Workout 7", "Workout 8", "Workout 9", "Workout 10", "Workout 11", "Workout 12", "Workout 13", "Workout 14"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, workouts_category);
        ListView WorkoutList = (ListView) findViewById(R.id.WorkoutList);
        WorkoutList.setAdapter(adapter);

        HomeButton = findViewById(R.id.HomeButton);

        HomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Intent intent = new Intent(WorkoutActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
